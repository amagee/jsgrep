var _ = require('underscore');
var chalk = require('chalk');
var espree = require('espree');
var fs = require('fs');
var escodegen = require('escodegen');


function getLines(fullText, code, callback) {
  var lineIndex = 1, columnIndex = 0;
  var tokenIndex = 0, commentIndex = 0;
  var currentLineStartIndex = 0;
  var currentLine = "";
  var currentLineTokens = [];
  var previousToken = null;

  function emitLine() {
    callback(currentLine, currentLineStartIndex, currentLineTokens);
    currentLineStartIndex += currentLine.length + 1;
    currentLine = "";
    currentLineTokens = [_.last(currentLineTokens)];
    lineIndex++;
    columnIndex = 0;
  }

  function doWhitespaceUntil(token) {
    if (previousToken != null) {
      doText(fullText.slice(previousToken.range[1], token.range[0]));
    }
  }

  function doText(text) {
    var lines = text.split("\n");
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];
      currentLine += line;
      if (i < lines.length - 1) {
        emitLine();
      }
    }
  }

  while (tokenIndex < code.tokens.length || commentIndex < code.comments.length) {
    var token;

    if (tokenIndex >= code.tokens.length || (
          commentIndex < code.comments.length && code.comments[commentIndex].start < code.tokens[tokenIndex].start
        )) {
      token = code.comments[commentIndex];
      doWhitespaceUntil(token);
      currentLineTokens.push(token);
      if (token.type === 'Line') {
        doText('//' + token.value);
      }
      else {
        doText('/*' + token.value + '*/');
      }
      commentIndex++;
    }
    else {
      token = code.tokens[tokenIndex];
      doWhitespaceUntil(token);
      currentLineTokens.push(token);
      doText(token.value);
      tokenIndex++;
    }
    lineIndex = token.loc.end.line;
    columnIndex = token.loc.end.column;
    previousToken = token;
  }
}


function highlightStringRange(s, start, end) {
  return (s.substring(0, start) 
    + chalk.red(s.substring(start, end))
    + s.substring(end, s.length)
  );
}


var input = fs.readFileSync('input.js', 'utf-8');
var code = espree.parse(input, { 
  raw: true, 
  tokens: true, 
  loc: true,
  range: true, 
  comment: true,
  ecmaVersion: 6,
  ecmaFeatures: {
    jsx: true
  }
});

const textTypes = ['JSXText', 'String', 'Template'];

let dollarRegex = /\$/g;
getLines(input, code, function(line, startIndex, tokens) {
  for (let i = tokens.length - 1; i >= 0; i--) {
    const token = tokens[i];
    if (_.include(textTypes, token.type)) {
      let matches = [], match;
      while (match = dollarRegex.exec(token.value)) {
        matches.push(match);
      }
      for (let j = matches.length - 1; j >= 0; j--) {
        const match = matches[j];
        if (match.index >= startIndex - token.range[0]) {
          if (token.type === 'Template' 
              && match.index === token.value.length - 2
              && token.value.substr(match.index, 2) === '${' 
              && token.value.substr(match.index - 1, 3) !== '\\${') {
            continue;
          }
          line = highlightStringRange(line, 
            token.range[0] + match.index - startIndex,
            token.range[0] + match.index + match.length - startIndex
          ); 
        }
      }
    }
  }
  console.log(line);
});

